// documemt.querySelector is used to retrieve an element from the webpage

/* 

document.getElementById('txt-first-name');
document.getElementByClassName('txt-input');
document.getElementByTagName('input');
*/
// const textFirstName= document.querySelector('#txt-first-name');
// const textLastName=  document.querySelector('#txt-last-name');
// const spanFullName = document.querySelector('#span-full-name');

// action is considered as an event

// textFirstName.addEventListener('keyup', (event) =>{

// 	spanFullName.innerHTML = textFirstName.value;
// });

// textLastName.addEventListener('keyup', (event) =>{

// 	spanFullName.innerHTML = textFirstName.value;
// });

// textFirstName.addEventListener('keyup', (event)=> {

// 	console.log(event.target);
// 	console.log(event.target.value);
// });


const textFirstName= document.querySelector('#txt-first-name');
const textLastName= document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

  function updateFullName() {
    const firstName = textFirstName.value;
    const lastName = textLastName.value;

    if (firstName.length === 0 || lastName.length === 0) {
      spanFullName.innerHTML = '';
    } else {
      const fullName = firstName + ' ' + lastName;
      spanFullName.innerHTML = fullName;
    }
  }

  textFirstName.addEventListener('keyup', updateFullName);
  textLastName.addEventListener('keyup', updateFullName);